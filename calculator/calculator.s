# $keyboard(0x9000) is the input
# $keyboardN is the input length

# $screen is the output on the screen
# 0x7010~0x701B is the first number
# 0x701C is the current operation
# 0x7020~0x702B is the second number
# (0 is one, 1 is ten, 2 is hundred...)

READ_KEY	ld		r0	    	$keyboard
		test		0x0	    	$rx
	    	jpc		<0>PRESSED
	    	test		0x1	    	$rx
	    	jpc		<1>PRESSED
	   	test		0x2	    	$rx
	    	jpc		<2>PRESSED
	    	test		0x3	    	$rx
	    	jpc		<+>PRESSED
	    	test		0x4	    	$rx
	   	jpc		<=>PRESSED
<0>PRESSED  	lda	        $rx	    	0x0000		# 歸零	
	    	st		$keyboard   	$rx		#

		lda		$ry		0x0004		#
		add		$ry		$rx		# 把存螢幕位數的register + 4		
		test		0x4		$ry		# 如果超過16, bit 4就會是1 => 不做事
		jpc		READ_KEY			# 回去讀鍵盤

								# 把之前螢幕顯示的數都往左移一格

								# 把最新的數加上去	

	    	ld		$rx	    	&keyboardN	# 把螢幕位數 +1
		lda		$ry		0x0001		#
		add		$rx		$ry		#
		
<1>PRESSED
<2>PRESSED
<+>PRESSED
<=>PRESSED			
