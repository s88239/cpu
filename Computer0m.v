module Computer0m(input clock, reset, 
			input [2:0] sw1,
			input [2:0] sw2,
         output [6:0] segPc1, output [6:0] segPc0, 
         output [6:0] segTick,
         output [6:0] segAlu1, output [6:0] segAlu0, output led, output [7:0] lcd, output lcden, output lcdrs, output lcdrw);
wire [31:0] counter;
wire [15:0] pc;
wire [3:0] tick;
wire [15:0] ir;
wire [15:0] mar;
wire [15:0] mdr;
wire [15:0] dbus;
wire [15:0] aluOut;
wire m_en, m_rw, carry;
wire [4:0] op;

Counter mCounter(clock, counter);

function3 lcd_decode(.clk(clock), .sw2(sw2), .sw1(sw1), .op(op), .lcd(lcd), .lcden(lcden), .lcdrs(lcdrs), .lcdrw(lcdrw) );

cpu0m cpu (.clock(counter[24]), .reset(reset), .pc(pc), .tick(tick), .ir(ir), .op(op),
.mar(mar), .mdr(mdr), .dbus(dbus), .m_en(m_en), .m_rw(m_rw), .aluOut(aluOut), .carry(led));

memory0m mem (.clock(counter[24]), .reset(reset), .en(m_en), .rw(m_rw), 
.abus(mar), .dbus_in(dbus), .dbus_out(dbus));

Seg7 mSegPc1(pc[7:4], segPc1);
Seg7 mSegPc0(pc[3:0], segPc0);
Seg7 mSegTick(tick, segTick);

Seg7 mSegAlu1(aluOut[7:4], segAlu1);
Seg7 mSegAlu0(aluOut[3:0], segAlu0);

endmodule

module cpu0m(input clock, reset, output reg [15:0] pc, 
             output reg [2:0] tick, output reg [15:0] ir,
					output reg [4:0] op,
                 output reg [15:0] mar, output reg [15:0] mdr,
                 inout [15:0] dbus, output reg m_en, m_rw,
                      output reg signed [15:0] aluOut, output reg carry);
   reg signed [7:0] imm;
	reg signed [10:0] location;
   reg signed [4:0] r;
   reg signed [15:0] A;
   reg signed [15:0] B;
	reg signed [15:0] tmpb;
	reg signed [15:0] tmpc;
   reg signed [11:0] cx12;
   reg signed [15:0] cx16;
   reg signed [23:0] cx24;
    reg signed [15:0] R [0:7];
    reg [2:0] ra;
    reg [2:0] rb;
	//reg carry;
	reg fetch_address;
	reg signed [31:0] mulVal;
    //`define PC R[7]
	reg signed [15:0] PC, SP;
	
	task read(input [15:0] addr);// read the data from addr
	begin
		mar = addr;
		m_rw = 1; // set read mode
		m_en = 1; // start reading
	end
	endtask
	
	task readDone(output [15:0] data);// get the data after reading
	begin
		mdr = dbus;
		data = mdr;
		m_en = 0; // close reading
	end
	endtask
	
	task write(input [15:0] addr,input [15:0] data);// write the data into addr
	begin
		mar = addr; // read the position from MDR
		mdr = data; // write the data into memory
		m_rw = 0; // open write mode
		m_en = 1; // start writing
	end
	endtask
	
   always @(posedge clock) begin
        if (reset) begin
            PC = 0;
            tick = 0;
				carry = 0;
          end
        else begin
            tick = tick+1;
            m_en = 0;
            case (tick)
            // Tick 1..3 memory.read(m[PC])
                1:    begin  // Tick 1: read the PC
						  read(PC);
                    PC = PC+2; // PC = PC + 4
                end
                2:    begin  // Tick 2: get the data from PC; ir = m[PC]
						  readDone(ir);
                    {op,ra,rb,r} = ir; // get the operation
                end
                3:    begin  // Tick 3: find the Register in ra and rb
                    A = R[ra];
                    B = R[rb];
                end
            // Tick 4..6 execute(IR)
                4:    begin  // Tick 4: PC 
                    case (op) // OP: Tick 4
                        5'h0: begin // Load Rx, #imm
									read(PC); // read next PC
									PC = PC + 2;
                        end
								5'h1: begin // Load Rx,[mem]
									read(PC); // read next PC
									PC = PC + 2; // PC change to next PC
								end
								5'h2: begin // Load Rx, [Ry]
									read(R[rb]);// read the data from the position stored in R[rb]
								end
								5'h3: begin // Store [mem], Rx
									read(PC);
									PC = PC+2; // PC change to next PC
								end
								5'h4: begin // Move Rx, Ry
									R[ra] = R[rb];
								end
								5'h5: begin // Swap Rx, Ry
									R[ra] = B; // store the value of R[rb] in R[ra]
									R[rb] = A; // store the value of R[ra] in R[rb]
								end
								5'h6: begin // Push Rx
									SP = R[ra];
									SP = SP -1;
								end
                        5'h7: begin // Add Rx,Ry
                            aluOut = A + B; // R[ra]+R[rb]
                        end
								5'h8: begin // Mul Rx, Ry
									mulVal = A * B; // store the result in the 32bit variable
									R[7] = mulVal[31:16]; // store high bit in R[7]
									R[6] = mulVal[15:0]; // store low bit in R[6]
									aluOut = R[6];
								end
								5'h9: begin // Pop Rx
									R[ra] = SP;
									SP = SP + 1;
								end
								5'hA: begin
									aluOut = A - B;// SUB ra rb R[ra] = R[ra] - R[rb]
								end
								5'hB: begin // Div Rx, Ry
									R[7] = A%B;
									R[6] = A/B;
								end
								5'hC: begin // AND Rx, Ry
									aluOut = A & B;
								end
								5'hD: begin // Xor Rx, Ry
									tmpb = ~A;
									tmpc = ~B;
									
									A = A & tmpc;
									B = B & tmpb;
									
									aluOut = A | B;
								end
								5'hE: begin // OR A, B
									aluOut = A | B;
								end
                        5'hF: begin // Jump Loc
									read(PC); // read next PC
									PC = PC+2; // PC change to next PC
                        end
								5'h10: begin // Jump.C Loc
									if(carry == 1)
									begin 
										read(PC); // read next PC
									end
									PC = PC+2; // PC change to next PC
								end
								5'h11: begin // Jump.NC Loc
									if(carry == 0)
									begin 
										read(PC); // read next PC
									end
									PC = PC+2; // PC change to next PC
								end
								5'h12: begin // Clear.C
                           carry = 0;
                        end
								5'h13: begin // Set.C
                           carry = 1;
                        end
								5'h14: begin // Test.biti Rx
									//rb bit i
									carry =  R[ra][rb] & 1'b1;
								end
                        default:    begin
                        end
                    endcase
                end
                5:    begin // ?? OP: Tick 5
                    case (op)
                        5'h0: begin // Load Rx, #imm
									readDone(ir);
									R[ra] = ir;
                        end
								5'h1: begin // Load Rx,[mem]
									readDone(ir);
									read(ir); // read the position from MDR
								end
								5'h2: begin // Load Rx, [Ry]
									readDone(ir);
									R[ra] = ir;
								end
								5'h3: begin // Store [mem], Rx
									readDone(ir);
									write(ir,R[ra]); // write R[ra] to [mem] at ir
								end
								5'h7: begin // Add Rx,Ry
									R[ra] = aluOut;
								end
                        5'h8: begin // MUL Rx,Ry
                            R[ra] = aluOut;
                        end
								5'hA: begin // Sub Rx,Ry
                            R[ra] = aluOut;
                        end
								5'hC: begin // And Rx,Ry
                            R[ra] = aluOut;
                        end
								5'hD: begin // Xor Rx, Ry
                            R[ra] = aluOut;
                        end
								5'hE: begin // Or Rx,Ry
                            R[ra] = aluOut;
                        end
                        5'hF: begin // Jump Loc
									readDone(ir);
									PC = ir;
                        end
								5'h10: begin // Jump.C Loc
									if(carry==1) begin
										readDone(ir);
										PC = ir;
									end
								end
								5'h11: begin // Jump.NC Loc
									if(carry==0) begin
										readDone(ir);
										PC = ir;
									end
								end
                        default:    begin
                        end
                    endcase
                end                
                6:    begin
						case(op)
							5'h1: begin // Load Rx,[mem]
								readDone(ir);
								R[ra] = ir;
							end
							5'h3: begin // Store [mem], Rx
								m_en = 0; // write done
							end
						endcase
                end
					 7:	begin
							tick = 0; // change 0 to restart fetching
					 end
                default:    begin
                end
            endcase
        end
        pc = PC;
    end
endmodule

module Seg7(input [3:0] num, output [6:0] seg);
   reg [6:0] nseg;
    always @(num) begin
        case (num)
            4'b0000: nseg = 7'b0111111; // 0
				4'b0001: nseg = 7'b0000110; // 1
            4'b0010: nseg = 7'b1011011; // 2
            4'b0011: nseg = 7'b1001111; // 3
            4'b0100: nseg = 7'b1100110; // 4
            4'b0101: nseg = 7'b1101101; // 5
            4'b0110: nseg = 7'b1111101; // 6
            4'b0111: nseg = 7'b0000111; // 7
            4'b1000: nseg = 7'b1111111; // 8
            4'b1001: nseg = 7'b1100111; // 9
            4'b1010: nseg = 7'b1110111; // A
            4'b1011: nseg = 7'b1111100; // b
            4'b1100: nseg = 7'b0111001; // C
            4'b1101: nseg = 7'b1011110; // d
            4'b1110: nseg = 7'b1111001; // E
            4'b1111: nseg = 7'b1110001; // F
            default: nseg = 7'b0000000; // dark
        endcase
    end
   assign seg = ~nseg;
endmodule

module Counter(input clock, output reg [31:0] counter);
always @(posedge clock) begin
    counter = counter + 1;
end
endmodule

module memory0m(input clock, reset, en, rw, 
            input [15:0] abus, input [15:0] dbus_in, output [15:0] dbus_out);
reg [7:0] m [0:128];
reg [15:0] data;

    always @(clock or reset or abus or en or rw or dbus_in) 
    begin
        if (reset == 1) begin
            {m[0],m[1]} = 16'b0000000000000000; // 00; 00000 000 00000000 LD   R1, #imm
            {m[2],m[3]} = 16'b0000000000000101; // 02; value 5
				//	R1 = 5
				{m[4],m[5]} = 16'b0000100100000000; // 04; 00000 001 00000000  LD   R2, [mem]
				{m[6],m[7]} = 16'b0000000000110010; // 06; [mem] at m[50]
				// R2 = 1
            {m[8],m[9]} = 16'b0011100000100000; // 08; 00111 000 001 00000 ADD  R1, R2
				// R1 = 6
				{m[10],m[11]} = 16'b1001100000000000; // 0A; 10011 00000000000 set carry
				// carry = 1
				{m[12],m[13]} = 16'b0001100100000000; // 0C; 00011 001 00000000 Store  [mem], R1
				{m[14],m[15]} = 16'b0000000000110100; // 0E; [mem] at m[52]
				// m[52] = 6;
				{m[16],m[17]} = 16'b0001001000000000; // 10; 00010 010 000 Load  R3, [R1]
				// R3 = [6] = 50 = 0x32
				{m[18],m[19]} = 16'b1001000000000000; // 12; 10010 00000000000 clear carry
				// carry = 0;
				{m[20],m[21]} = 16'b0101000000100000; // 14; 01010 000 001 00000 SUB   R1, R2
				// R1 = R1 - R2 = 6 - 1 = 5
				{m[22],m[23]} = 16'b0000100100000000; // 16; 00000 001 00000000  LD   R2, [mem]
				{m[24],m[25]} = 16'b0000000000110100; // 18; [mem] at m[52]; m[52] = 6;
				// R2 = 6
				{m[26],m[27]} = 16'b0100000000100000; // 1A; 01000 000 001 00000 MUL   R1, R2
				// R1 = R1 * R2 = 5*6 = 30 = 0x1E
				{m[28],m[29]} = 16'b0011101000100000; // 1C; 00111 010 001 00000 LOOP: ADD   R3, R2
				// R3 = R3 + R2 = 50 + 6n = 0x38 + 0x6(n-1)
            {m[30],m[31]} = 16'b0111100000000000; // 1E; 01111 00000000000 JMP  LOOP
				{m[32],m[33]} = 16'b0000000000011100; // 20; pc address; JUMP to m[28]
				{m[50],m[51]} = 16'b0000000000000001; // 32; value 1
				//{m[52],m[53]} = 16'b0000000000000011; // 34; value 3
            data = 16'hZZZZ; 
        end else if (abus >=0 && abus < 128) begin
            if (en == 1 && rw == 0) // r_w==0:write
            begin
                data = dbus_in;
                {m[abus], m[abus+1]} = dbus_in;
            end
            else if (en == 1 && rw == 1) // r_w==1:read
                data = {m[abus], m[abus+1]};
            else
                data = 16'hZZZZ;
        end else
            data = 16'hZZZZ;
    end
    assign dbus_out = data;
endmodule

module function3(input clk,input [2:0] sw2 ,input [2:0] sw1 ,input [4:0] op,output [7:0] lcd,output lcden,output lcdrs,output lcdrw);
	//input clk;
	//input [2:0] sw1;
	//input [2:0] sw2;
	//input [4:0] button;
   //output [7:0] lcd;
	//output lcden;
   //output lcdrs;
   //output lcdrw;
	
	reg [7:0] lcd_words;
	reg [31:0] t;
	reg [7:0] pos;
	reg [2:0] pre_sw1;
	reg [2:0] pre_sw2;
	reg lcden_reg;
	reg lcdrs_reg;
	reg lcdrw_reg;
	reg [127:0] words;
	reg [1:0] state;
	reg [8:0] line;
	reg [4:0] op_reg ;
	reg [4:0] preop;
	assign lcd = lcd_words;
	assign lcden = lcden_reg;
	assign lcdrs = lcdrs_reg;
	assign lcdrw = lcdrw_reg;
	parameter
		instruction_1 = "LOAD Rx  #imm   ",
		instruction_2 = "LOAD Rx  [imm]  ",
		instruction_3 = "LOAD Rx  [Ry]   ",
		instruction_4 = "Store [mem] Rx  ",
		instruction_5 = "Move Rx Ry      ",
		instruction_6 = "Swap Rx Ry      ",
		instruction_7 = "Push Rx         ",
		instruction_8 = "Add Rx,Ry       ",
		instruction_9 = "Mul Rx, Ry      ",
		instruction_A = "Pop Rx          ",
		instruction_B = "Sub Rx,Ry       ",
		instruction_C = "Div Rx, Ry      ",
		instruction_D = "And Rx,Ry       ",
		instruction_E = "Xor Rx, Ry      ",
		instruction_F = "Or Rx,Ry        ",
		instruction_10 = "Jump Loc       ",
	   instruction_11 = "Jump.C Loc      ",
 	   instruction_12 = "Jump.NC Loc     ",
		instruction_13 = "Clear.C         ",
		instruction_14 = "Set.C           ",
		instruction_15 = "Test.biti Rx    ",
		
		
		reset_state = 2'b00,
		cleared_state = 2'b01,
		moved_state = 2'b10,
		displayed_state = 2'b11;
	
initial 
begin 
			pre_sw1 = 3'b000;
			pre_sw2 = 3'b000;
			
end

always @(posedge clk)
begin
	/*if(button[0] != 1'b1)
	begin
			;//msg_cnt=0;
	end
	else
	begin*/
	
		if(sw1[0] != 1'b0)	//(sw1 != pre_sw1 || (sw1 == 0 && sw2 != pre_sw2))0
			begin
				reset();
				set_words();
				//pre_sw1 = sw1;
				//pre_sw2 = sw2;
		end
		else if(state == reset_state) set_words();
		
		display_process(3'b100,	7'b0000000, 16, 1000000000);
		/*case(sw1)
			0:	
			
			1: display_process(3'b100, 7'b0000000, 16, 1000000000);
			2: display_process(3'b100, 7'b1000000, 16, 1000000000);
			3:	
				case(state)
						reset_state: clean_screen(3'b100);
				endcase
			4:	
			begin
				case(line)
					0: display_process(3'b100, 7'b0000000, 16, 1000000);
					1:
						begin
								case(state)
									reset_state:	move(7'b1000000);
									moved_state:	print(1250000,16);
								endcase
								
						end
				endcase
			end
			5:	if(line < 16)  display_process(3'b111, line[6:0],  0,  50000000);
			6:	if(line < 16)	display_process(3'b111, {3'b100,4'b1111-line[3:0]}, 0, 50000000);
			7:	
			begin
				if(line < 256)	
				begin
					display_process(3'b100, 7'b1000000, 16, 50000000);
				end
			end
		endcase
		*/
		
		t = t + 1;
	//end

end


task moveleft;
begin
	case(t)
		28'd50000:	lcd_words = 8'b00001111;
		28'd60000:	lcden_reg = 1'b1;
		28'd70000:	lcden_reg = 1'b0;
	endcase
end
endtask

task set_words;
begin
	case(op)
	5'b00000:	words = instruction_1;
	5'b00001:	words = instruction_2;
	5'b00010:	words = instruction_3;
	5'b00011:	words = instruction_4;
	5'b00100:	words = instruction_5;
	5'b00101:	words = instruction_6;
	5'b00110:	words = instruction_7;
	5'b00111:	words = instruction_8;
	5'b01000:	words = instruction_9;
	5'b01001:	words = instruction_A;
	5'b01010:	words = instruction_B;	
	5'b01011:	words = instruction_C;
	5'b01100:	words = instruction_D;
	5'b01101:	words = instruction_E;
	5'b01110:	words = instruction_F;
	5'b01111:	words = instruction_10;
	5'b10000:	words = instruction_11;
	5'b10001:	words = instruction_12;
	5'b10010:	words = instruction_13;
	5'b10011:	words = instruction_14;
	
	
	endcase
end
endtask

function [7:0] hexdit;
input [3:0] number;

begin
case(number)
0: hexdit = "0";	
1:	hexdit = "1";
2:	hexdit = "2";
3:	hexdit = "3";
4:	hexdit = "4";
5:	hexdit = "5";
6:	hexdit = "6";
7:	hexdit = "7";
8:	hexdit = "8";
9:	hexdit = "9";
10: hexdit = "A";
11: hexdit = "B";
12: hexdit = "C";
13: hexdit = "D";
14: hexdit = "E";
15: hexdit = "F";
endcase
end
endfunction


task display_process;
input [2:0] cursor;
input [6:0] address;
input [7:0]	length;
input [31:0] sleep;

begin
	case(state)
		reset_state: clean_screen(cursor);
		cleared_state:	move(address);
		moved_state:	print(1250000,length);
		displayed_state:	take_rest(sleep);
	endcase
end
endtask

task move;
input [6:0]address;
begin
	case(t)
	28'd50000:	lcd_words = 8'b00000010;
	28'd60000:	lcden_reg = 1'b1;
	28'd70000:	lcden_reg = 1'b0;
	
	28'd170000:	lcd_words = {1'b1, address};
	28'd180000: lcden_reg = 1'b1;
	28'd190000: lcden_reg = 1'b0;
	28'd200000:	
		begin
			state = moved_state;
			t = 0;
		end
	endcase
end
endtask

task reset;
begin
	state = reset_state;
	lcden_reg = 1'b0;
	lcdrs_reg = 1'b0;
	lcdrw_reg = 1'b0;
	pos = 7'b0000000;
	t  = 0;
	line = 0;
end
endtask

task clean_screen;
input [2:0] cursor;

begin
case(t)
	28'd50000:	lcd_words = 8'b00000001;
	28'd60000:	lcden_reg = 1'b1;
	28'd70000:	lcden_reg = 1'b0;
	
	28'd170000: lcd_words = {5'b00001,cursor};
	28'd180000:	lcden_reg = 1'b1;
	28'd190000:	lcden_reg = 1'b0;
	28'd200000: 
		begin
			state = cleared_state;
			t = 0;
		end
endcase
end
endtask

task print;
input [27:0] delay;
input [7:0] length;
	
begin
	if(t == delay)
	begin
		t = 0;
	end
	else 
	begin
		if(pos < length)
		begin
			case(t)
				28'd50000: lcdrs_reg = 1'b1;
				28'd60000: 
					begin
					lcd_words = words[127:120];
					words = words << 8;
					end
				28'd70000: lcden_reg = 1'b1;
				28'd80000:
				begin
					lcden_reg = 1'b0;
					pos = pos + 1;
				end
			endcase
		end
		
		else
		begin
			state = displayed_state;
			lcdrs_reg = 1'b0;
			//pos = 0;
			t = 0;
		end
	end
end
endtask

task take_rest;
input [31:0] sleep;
begin
	if(t == sleep)
	begin
		state = reset_state;
		pos = 0;
		t = 0;
		line = line + 1;
	end
end
endtask

endmodule
