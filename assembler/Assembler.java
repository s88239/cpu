import java.io.PrintWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Assembler{
	public static void main(String[] args){
		Scanner keyboard = new Scanner(System.in);
		Scanner fileIn = null;
		PrintWriter fileOut = null;

		System.out.println("Enter input file name");
		String fileInName = keyboard.next();
		System.out.println("Enter output file name");
		String fileOutName = keyboard.next();
		try{
			fileIn = new Scanner(new FileInputStream(fileInName));
		}
		catch (FileNotFoundException e){
			System.out.println("File not found");
		}


		try{
			fileOut = new PrintWriter(new FileOutputStream(fileOutName));
		}
		catch (FileNotFoundException e){
			System.out.println("Error opening ther file");
		}

		String readLine;
		String currentToken;
		String[] labels = new String[20];
		int[] labelLine = new int[20];
		int labelNumber = 0;
		int lineCount = 0;
		int tempR = 0;
		int tempLine = 0;
		String tempString, tempString2;
		while(fileIn.hasNextLine()){
			readLine = fileIn.nextLine();
			StringTokenizer wordCut = new StringTokenizer(readLine);
			while(wordCut.hasMoreTokens()){
				currentToken = wordCut.nextToken();
				if(currentToken.equals("lda")){
					fileOut.print("00000");
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					currentToken = wordCut.nextToken();
					fileOut.println("00000000\n" + currentToken);
				}else if(currentToken.equals("ld")){
					fileOut.print("00001");
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					currentToken = wordCut.nextToken();
					fileOut.println("00000000\n" + currentToken);
				}else if(currentToken.equals("ldi")){
					fileOut.print("00010");
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					currentToken = wordCut.nextToken();
					fileOut.println("00000000\n" + currentToken);
				}else if(currentToken.equals("st")){
					fileOut.print("00011");
					currentToken = wordCut.nextToken();
					tempString2 = currentToken;
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					fileOut.println("00000000\n" + tempString2);
				}else if(currentToken.equals("mv")){
					fileOut.print("00100");
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					fileOut.println("00000");
				}else if(currentToken.equals("swap")){
					fileOut.print("00101");
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					fileOut.println("00000");
				}else if(currentToken.equals("push")){
					fileOut.print("00110");
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					fileOut.println("00000000");
				}else if(currentToken.equals("pop")){
					fileOut.print("00111");
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					fileOut.println("00000000");
				}else if(currentToken.equals("add")){
					fileOut.print("01000");
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					fileOut.println("00000");
				}else if(currentToken.equals("sub")){
					fileOut.print("01001");
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					fileOut.println("00000");
				}else if(currentToken.equals("mul")){
					fileOut.print("01010");
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					fileOut.println("00000");
				}else if(currentToken.equals("div")){
					fileOut.print("01011");
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					fileOut.println("00000");
				}else if(currentToken.equals("and")){
					fileOut.print("01100");
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					fileOut.println("00000");
				}else if(currentToken.equals("xor")){
					fileOut.print("01101");
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					fileOut.println("00000");
				}else if(currentToken.equals("or")){
					fileOut.print("01110");
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					fileOut.println("00000");
				}else if(currentToken.equals("jp")){
					fileOut.print("01111");
					fileOut.println("00000000000");
					currentToken = wordCut.nextToken();
					tempLine = searchLabel(labels, currentToken);
					if(tempLine>=0){
						tempString = Integer.toBinaryString(labelLine[tempLine]);
						fileOut.println("0000000000000000".substring(tempString.length()) + tempString);
					}
				}else if(currentToken.equals("jpc")){
					fileOut.print("10000");
					fileOut.println("00000000000");
					currentToken = wordCut.nextToken();
					tempLine = searchLabel(labels, currentToken);
					if(tempLine>=0){
						tempString = Integer.toBinaryString(labelLine[tempLine]);
						fileOut.println("0000000000000000".substring(tempString.length()) + tempString);
					}
				}else if(currentToken.equals("jpnc")){
					fileOut.print("10001");
					fileOut.println("00000000000");
					currentToken = wordCut.nextToken();
					tempLine = searchLabel(labels, currentToken);
					if(tempLine>=0){
						tempString = Integer.toBinaryString(labelLine[tempLine]);
						fileOut.println("0000000000000000".substring(tempString.length()) + tempString);
					}
				}else if(currentToken.equals("clrc")){
					fileOut.print("10010");
					fileOut.println("00000000000");
				}else if(currentToken.equals("setc")){
					fileOut.print("10011");
					fileOut.println("00000000000");
				}else if(currentToken.equals("test")){
					fileOut.print("10100");
					currentToken = wordCut.nextToken();
					tempR = new Integer(currentToken.substring(1));
					tempString = Integer.toBinaryString(tempR);
					fileOut.print("000".substring(tempString.length()) + tempString);
					fileOut.println("00000000");
				}else{
					labels[labelNumber] = currentToken;
					labelLine[labelNumber] = lineCount;
					labelNumber++;
				}
				//System.out.println(currentToken);
			}
			lineCount++;
		}
		fileOut.close();
	}
	public static int searchLabel(String[] labels, String theLabel){
		for(int i=0;i<labels.length;i++){
			if(labels[i].equals(theLabel)){
				return i;
			}
		}
		System.out.println("Error!! No " + theLabel + " label...");
		return -1;
	}
}
